#!/usr/bin/nodejs

var URL = require("url");
var SerialPort = require("serialport").SerialPort;

var five = require("johnny-five");

/********************************************************************************/

var adapters = require('./adapters/__index.js');

var running = true, link = URL.parse(process.argv[2]);

var narrow = {
    protocol: link.protocol.substr(0, -1),
    identity: link.auth,
    endpoint: link.host,
    channel:  link.pathname,
    module:   link.hash.substr(1).split('.'),
};

console.log(narrow);

/********************************************************************************/

var thing = adapters;

for (var i=0 ; i<narrow.module.length ; i++) {
    thing = thing[narrow.module[i]];
}

var board = new five.Board({
    port: new SerialPort(narrow.channel, {
        baudrate: 9600,
        buffersize: 1
    })
});

/********************************************************************************/

board.on("ready", function() {
    var jarvis = thing.initialize(board);

    jarvis.narrow  = narrow;
    jarvis.wrapper = thing;

    jarvis.board   = board;
    jarvis.io      = thing.pinout;

    if (thing.setup) {
        thing.setup(jarvis);
    }

    while (running==true) {
        if (thing.populate) {
            thing.populate(jarvis, jarvis.dev, jarvis.io, function (field, payload) {
                console.log('-> ' + field + ' = ' + payload);
            });
        }

        if (thing.loop) {
            thing.loop(jarvis);
        }
    }
});

