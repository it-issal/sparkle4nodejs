var five = require("johnny-five");

exports.pinout = {
    // Analog
    'ldr':    'A0',
    'noise':  'A2',

    // Digital
    'move':   2,
    'dht':    4,

    'out-1':  12,
    'out-2':  13,

    // PWM
    'rgb':    [3, 5, 6],
    'buzzer': 9,

    'cam-x':  10,
    'cam-y':  11,

    // Special
    'ir-rx':  7,
    'ir-tx':  8,
};

/**********************************************************************/

exports.initialize = function (board) {
    return {
        'foo': 'bar',
    };
};

/**********************************************************************/

exports.setup = function (jarvis) {
    jarvis.dev.pinMode(jarvis.io['ldr'], five.Pin.ANALOG);

    jarvis.dev.pinMode(jarvis.io['buzzer'], five.Pin.PWM);

    jarvis.dev.pinMode(jarvis.io['rgb'][0], five.Pin.PWM);
    jarvis.dev.pinMode(jarvis.io['rgb'][1], five.Pin.PWM);
    jarvis.dev.pinMode(jarvis.io['rgb'][2], five.Pin.PWM);

    //board.pinMode(jarvis.io['ir-tx'],  five.Pin.PWM);

    /////////////////////////////////////////////////////////////////////

    jarvis.dev.hook('ir_code');
};

exports.loop = function (jarvis) {
    
};

/**********************************************************************/

exports.populate = function (jarvis, dev, io, emit) {
    jarvis.dev.analogRead(jarvis.io['ldr'], function(voltage) {
        emit('ldr', voltage);
    });
};

/**********************************************************************/

exports.buzzer = function (jarvis, length=255) {
    jarvis.dev.analogWrite(jarvis.io['buzzer'], length);
};

/**********************************************************************/

exports.on_ir_code = function (jarvis) {
};

